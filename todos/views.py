from django.shortcuts import render, get_object_or_404
from todos.models import TodosList

def todo_list_list(request):
    todo_lists = TodosList.objects.all()
    context = {'todo_lists': todo_lists}
    return render(request, 'todo/todo_list.html', context)

def todo_list_details(request, id):
    todo_lists = TodosList.objects.get(id=id)
    context = {
        'todo_lists': todo_lists,
    }

    return render(request, 'todo/detail.html', context)
