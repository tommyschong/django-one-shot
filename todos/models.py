from django.db import models

# Create your models here.
class TodosList(models.Model):
    name = models.CharField(max_length=100)
    created_on=models.DateTimeField(auto_now_add=True)

class TodoItem(models.Model):
    task = models.CharField(max_length=100)
    due_date = models.DateTimeField()
    is_completed = models.BooleanField(default=False)
    list = models.ForeignKey(TodosList, on_delete=models.CASCADE)
